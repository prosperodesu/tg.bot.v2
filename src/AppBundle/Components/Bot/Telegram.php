<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.10.17
 * Time: 14:09
 */

namespace AppBundle\Components\Bot;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class Bot
 */
class Telegram
{
    protected $baseUrl;
    protected $channelKey;
    protected $url = 'https://api.telegram.org/bot';
    protected $botToken = '441498198:AAHlqC_s5DI07c1BXTTsHmpEPk0OHITz4Eo';

    /**
     * Bot constructor.
     * @param $name
     * @param bool $from
     * @param bool $key
     */
    public function __construct($channelKey = false)
    {
        $this->baseUrl = $this->url . $this->botToken;

    }

    /**
     * @param $message
     * @param $chatId
     */
    public function sendMessage($message, $chatId)
    {

        $this->sendRequest($this->baseUrl . '/sendMessage', [
            'chat_id' => $chatId,
            'text' => $message
        ]);
    }

    /**
     * @param $message
     */
    public function sendAll($message)
    {

    }

    /**
     * @param $command
     */
    public function command($command)
    {

    }

    /**
     * Subscribe user
     */
    public function subscribe()
    {


    }

    /**
     * @throws \Exception
     * @throws \Throwable
     */
    public function unsubscribe()
    {

    }

    /**
     * Send id to user
     */
    public function sendId()
    {

    }

    /**
     * @param $url
     * @param $params
     */
    public function sendRequest($url, $params)
    {

        return $this->callRequest('GET', $url, $params);
    }

    public function callRequest($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.10.17
 * Time: 14:09
 */

namespace AppBundle\Components\shoko;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class Bot
 */
class ShokoPromo
{
    protected $url = 'http://new.shoko-service.ru/code/';

    public function check($code)
    {
        $params = [
            'promo_code' => $code
        ];

        $response = json_decode($this->sendPostRequest($this->url, $params), true);

        if ($response['code_accepted']) {
            return 'Код одобрен';
        } else {
            return 'Код отклонен';
        }
    }

    /**
     * @param $url
     * @param $params
     */
    public function sendRequest($url, $params)
    {

        return $this->callRequest('GET', $url, $params);
    }

    /**
     * @param $url
     * @param $params
     */
    public function sendPostRequest($url, $params)
    {

        return $this->callRequest('POST', $url, $params);
    }

    public function callRequest($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}
<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Channel;
use AppBundle\Entity\Subscriber;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Components\Bot\Telegram as TelegramBot;

/**
 * Class ApiController
 * @package AppBundle\Controller
 */
class ApiController extends Controller
{
    /**
     * @Route("/api/channels/link/{subscriberId}/{channelId}", name="link_subscriber_to_channel")
     */
    public function linkSubscriberToChannel(Request $request, $channelId, $subscriberId)
    {
        $bot = new TelegramBot();

        $repositoryChannel = $this->getDoctrine()->getRepository(Channel::class);
        $channel = $repositoryChannel->find($channelId);

        $repositorySubscriber = $this->getDoctrine()->getRepository(Subscriber::class);
        $subscriber = $repositorySubscriber->find($subscriberId);

        $subscriber->addChannel($channel);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($subscriber);
        $manager->flush();

        $bot->sendMessage('@Администратор подписал вас на канал: ' . $channel->getName(), $subscriber->getChatId());

        return $this->redirectToRoute('channel_link_control', ['channelId' => $channel->getId()]);
    }

    /**
     * @Route("/api/channels/unlink/{subscriberId}/{channelId}", name="unlink_subscriber_from_channel")
     */
    public function unlinkSubscriberFromChannel(Request $request, $channelId, $subscriberId)
    {
        $bot = new TelegramBot();

        $repositoryChannel = $this->getDoctrine()->getRepository(Channel::class);
        $channel = $repositoryChannel->find($channelId);

        $repositorySubscriber = $this->getDoctrine()->getRepository(Subscriber::class);
        $subscriber = $repositorySubscriber->find($subscriberId);

        $subscriber->removeFromChannel($channel);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($subscriber);
        $manager->flush();

        $bot->sendMessage('@Администратор удалил вас из канала: ' . $channel->getName(), $subscriber->getChatId());

        return $this->redirectToRoute('channel_link_control', ['channelId' => $channel->getId()]);
    }
}
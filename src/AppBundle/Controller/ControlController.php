<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Channel;
use AppBundle\Entity\Subscriber;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ControlController
 * @package AppBundle\Controller
 */
class ControlController extends Controller
{
    /**
     * @Route("/control/channels", name="channels_control")
     */
    public function controlChannelsAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Channel::class);
        $channels = $repository->findAll();

        return $this->render('control/channels.html.twig', ['channels' => $channels]);
    }

    /**
     * @Route("/control/channels/add", name="channels_add")
     */
    public function addChannelAction(Request $request)
    {

        // create a task and give it some dummy data for this example
        $channel = new Channel();

        $form = $this->createFormBuilder($channel)
            ->add('name', TextType::class, [
                    'label' => 'Название',
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'Введите имя канала'
                    ]
                ]
            )
            ->add('save', SubmitType::class, [
                'label' => 'Создать',
                'attr' => [
                    'class' => 'btn btn-success waves-effect waves-light m-r-10'
                ]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $channel->setKey(uniqid());

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($channel);
            $manager->flush();

            return $this->redirectToRoute('channels_control');
        }

        // replace this example code with whatever you need
        return $this->render('control/add.channel.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/control/channels/link/{channelId}", name="channel_link_control")
     */
    public function linkChannelsAction(Request $request, $channelId)
    {
        $repository = $this->getDoctrine()->getRepository(Channel::class);
        $channel = $repository->find($channelId);

        $repositorySubscriber = $this->getDoctrine()->getRepository(Subscriber::class);
        $subscribers = $repositorySubscriber->findAll();

        return $this->render('control/channel.link.html.twig', ['channel' => $channel, 'subscribers' => $subscribers]);
    }


    /**
     * @Route("/control/subscribers", name="subscribers_control")
     */
    public function controlSubscribersAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Subscriber::class);
        $subscribers = $repository->findAll();

        return $this->render('control/subscribers.html.twig', ['subscribers' => $subscribers]);
    }
}
<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Subscriber;
use AppBundle\Entity\Channel;
use AppBundle\Entity\History;
use AppBundle\Components\Bot\Telegram as TelegramBot;
use AppBundle\Components\shoko\ShokoPromo;
use AppBundle\Websocket\Stats;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->redirect('stats');
    }

    /**
     * @Route("/stats", name="stats_page")
     */
    public function statsAction(Request $request)
    {

        $manager = $this->getDoctrine()->getManager();

        // notifications by day
        $date24 = new \DateTime();
        $date24->modify('-24 hour');

        $qb = $manager->createQueryBuilder();
        $qb->select('count(history.id)');
        $qb->andWhere('history.createdAt > :date');
        $qb->setParameter(':date', $date24);
        $qb->from('AppBundle:History', 'history');

        $countLast24Hours = $qb->getQuery()->getSingleScalarResult();

        // notifications by hour
        $dateHour = new \DateTime();
        $dateHour->modify('-1 hour');

        $qb = $manager->createQueryBuilder();
        $qb->select('avg(history.id)');
        $qb->andWhere('history.createdAt > :date');
        $qb->setParameter(':date', $dateHour);
        $qb->from('AppBundle:History', 'history');

        $countLastHour = (int)$qb->getQuery()->getSingleScalarResult();

        // notifications total
        $qb = $manager->createQueryBuilder();
        $qb->select('count(history.id)');
        $qb->from('AppBundle:History', 'history');

        $countTotal = $qb->getQuery()->getSingleScalarResult();

        // subscribers
        $qb = $manager->createQueryBuilder();
        $qb->select('count(subscriber.id)');
        $qb->from('AppBundle:Subscriber', 'subscriber');

        $subscribersTotal = $qb->getQuery()->getSingleScalarResult();

        // graph

        $repository = $this->getDoctrine()->getRepository(Channel::class);
        $channels = $repository->findAll();

        $graphData = [
            'titles' => [],
            'dates' => [],
            'counts' => [],
        ];

        foreach ($channels as $channel) {

            $tempCounts = [];

            $sql = "SELECT DATE(t1.created_at), count(t1.id)
                      FROM history as t1
                JOIN channels as t2
                ON t1.channel_id = t2.id
                WHERE t1.channel_id = {$channel->getId()}
                GROUP BY DATE(t1.created_at)
                ORDER BY DATE(t1.created_at)
              ";

            $em = $this->getDoctrine()->getManager();
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll();

            foreach ($data as $row) {
                if (!in_array($row['date'], $graphData['dates'])) {
                    $graphData['dates'][] = $row['date'];
                }
                $tempCounts[] = $row['count'];
            }

            $graphData['counts'][] = $tempCounts;
        }

        return $this->render('default/index.html.twig', [
            'countLast24Hours' => $countLast24Hours,
            'countLastHour' => $countLastHour,
            'countTotal' => $countTotal,
            'subscribersTotal' => $subscribersTotal,
            'graphDataResult' => $graphData,
            'channels' => $channels
        ]);
    }

    /**
     * @Route("/callback", name="callbackIndex")
     */
    public function callbackIndexAction()
    {
        $data = json_decode(file_get_contents('php://input'));

        /*$myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
        $txt = json_encode($data);
        fwrite($myfile, $txt);
        fclose($myfile);

        exit();*/

        $firstName = 'John';
        $lastName = 'Doe';
        $username = 'Default username';

        $chatId = $data->message->chat->id;
        $message = $data->message->text;

        if (isset($data->message->chat->first_name)) {
            $firstName = $data->message->chat->first_name;
        }
        if (isset($data->message->chat->last_name)) {
            $lastName = $data->message->chat->last_name;
        }
        if (isset($data->message->chat->username)) {
            $username = $data->message->chat->username;
        }

        $bot = new TelegramBot();
        $shokoPromo = new ShokoPromo();

        $repository = $this->getDoctrine()->getRepository(Subscriber::class);
        $subscriber = $repository->findOneBy(['chatId' => $chatId]);

        /** command with arguments */
        if (strlen($message) >= 5 and strlen($message) <= 8 and strpos($message, '/') !== 0) {
            $result = $shokoPromo->check($message);
            $bot->sendMessage('@' . $result, $chatId);
        }

        /** commands without arguments */
        switch ($message) {
            case '/subscribe':
                if (!$subscriber) {

                    $manager = $this->getDoctrine()->getManager();

                    $subscriber = new Subscriber();
                    $subscriber->setName('New subscriber');
                    $subscriber->setChatId($chatId);
                    $subscriber->setUsername($username);
                    $subscriber->setName("{$firstName} {$lastName}");
                    $subscriber->setPin(rand(10000, 9990000));

                    $manager->persist($subscriber);
                    $manager->flush();

                    $bot->sendMessage('@Вы успешно подписались на уведомления!', $chatId);
                    $bot->sendMessage("@Ваш ID: {$subscriber->getPin()}", $chatId);
                } else {
                    $bot->sendMessage('@Вы уже подписаны на уведомления!', $chatId);
                }
                break;
            case '/unsubscribe':
                if ($subscriber) {
                    $manager = $this->getDoctrine()->getManager();
                    $manager->remove($subscriber);
                    $manager->flush();
                    $bot->sendMessage('@Вы успешно отписались от уведомлений!', $chatId);
                } else {
                    $bot->sendMessage('@Вы еще не подписаны на уведомления. Введите /subscribe для подписки!', $chatId);
                }
                break;
            case '/start':
                $bot->sendMessage('@Ок', $chatId);
                break;
            default:
                break;
        }

        return new JsonResponse(['status' => 'ok']);
    }


    /**
     * @Route("/callback/send", name="callback")
     */
    public function callbackAction(Request $request)
    {
        $bot = new TelegramBot();

        $key = $request->get('key');
        $message = $request->get('message');

        $repository = $this->getDoctrine()->getRepository(Channel::class);

        $channel = $repository->findOneBy(
            ['key' => $key]
        );

        if ($channel) {

            $subscribers = $channel->getSubscribers();

            foreach ($subscribers as $subscriber) {
                $bot->sendMessage($message, $subscriber->getChatId());
            }

            $history = new History();
            $history->setMessage($message);
            $history->setChannel($channel);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($history);
            $manager->flush();

            // notifications by day
            $date24 = new \DateTime();
            $date24->modify('-24 hour');

            $qb = $manager->createQueryBuilder();
            $qb->select('count(history.id)');
            $qb->andWhere('history.createdAt > :date');
            $qb->setParameter(':date', $date24);
            $qb->from('AppBundle:History', 'history');

            $totalLast24Hours = $qb->getQuery()->getSingleScalarResult();

            // notifications by hour
            $dateHour = new \DateTime();
            $dateHour->modify('-1 hour');

            $qb = $manager->createQueryBuilder();
            $qb->select('avg(history.id)');
            $qb->andWhere('history.createdAt > :date');
            $qb->setParameter(':date', $dateHour);
            $qb->from('AppBundle:History', 'history');

            $avgLastHour = $qb->getQuery()->getSingleScalarResult();

            // notifications total
            $qb = $manager->createQueryBuilder();
            $qb->select('count(history.id)');
            $qb->from('AppBundle:History', 'history');

            $countTotal = $qb->getQuery()->getSingleScalarResult();

            // subscribers
            $qb = $manager->createQueryBuilder();
            $qb->select('count(subscriber.id)');
            $qb->from('AppBundle:Subscriber', 'subscriber');

            $subscribersTotal = $qb->getQuery()->getSingleScalarResult();

            $pusher = $this->container->get('gos_web_socket.wamp.pusher');
            $pusher->push(
                [
                    'action' => 'need_plus_stats',
                    'params' => [
                        'nowDay' => $totalLast24Hours,
                        'avgLastHour' => $avgLastHour,
                        'total' => $countTotal,
                        'subscribersTotal' => $subscribersTotal
                    ]
                ],
                'app_websocket_stats'
            );

            return new JsonResponse(['status' => 'ok']);
        }

        return new JsonResponse(['status' => 'channel_not_found']);
    }
}

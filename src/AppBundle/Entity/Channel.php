<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Channel
 *
 * @ORM\Entity
 * @ORM\Table(name="channels")
 */
class Channel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $key;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Subscriber", mappedBy="channels")
     */
    private $subscribers;

    /**.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\History", mappedBy="channel")
     */
    private $notifications;

    public function __construct()
    {
        $this->subscribers = new ArrayCollection();
        $this->notiications = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getCallbackUrl()
    {
        return "https://notify.ecsv.org.ua/callback/send?message=<-->&key={$this->getKey()}";
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function addSubscriber(\AppBundle\Entity\Subscriber $subscriber)
    {
        $this->subscribers[] = $subscriber;

        return $this;
    }

    public function getSubscribers()
    {
        return $this->subscribers;
    }

    public function getNotifications()
    {
        return $this->notifications;
    }
}
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Channel
 *
 * @ORM\Entity
 * @ORM\Table(name="subscribers")
 */
class Subscriber
{
    const IS_WAIT_ACCEPT = 0;
    const IS_ACCEPTED = 1;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $chatId;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $pin;

    /**
     *
     * @ORM\Column(name="is_accept", type="boolean")
     */
    protected $isAccept = self::IS_WAIT_ACCEPT;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Channel", inversedBy="subscribers")
     * @ORM\JoinTable(name="subscribers_to_channels")
     */
    private $channels;

    public function __construct()
    {
        $this->channels = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPin()
    {
        return $this->pin;
    }

    public function getChatId()
    {
        return $this->chatId;
    }

    public function getIsAccept()
    {
        if ($this->isAccept == self::IS_ACCEPTED) {
            return 'Активен';
        } else if ($this->isAccept == self::IS_WAIT_ACCEPT) {
            return 'Неактивен';
        }
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    public function setChatId($chatId)
    {
        $this->chatId = $chatId;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setPin($pin)
    {
        $this->pin = $pin;
    }

    public function getChannels()
    {
        return $this->channels;
    }

    public function isSubscribeToChannel(\AppBundle\Entity\Channel $channel)
    {
        if ($this->channels->contains($channel)) {
            return true;
        } else {
            return false;
        }
    }

    public function addChannel(\AppBundle\Entity\Channel $channel)
    {
        if (!$this->channels->contains($channel)) {
            $this->channels[] = $channel;
        }

        return $this;
    }

    public function removeFromChannel(\AppBundle\Entity\Channel $channel)
    {
        return $this->channels->removeElement($channel);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.12.17
 * Time: 15:47
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Subscriber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SubscriberRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Subscriber::class);
    }

    public function findOneByChatId($chatId)
    {

        $manager = $this->getEntityManager();

        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.chatId = :chatId')
            ->setParameter('chatId', $chatId)
            ->getQuery();

        if (!$qb->execute()) {
            $subscriber = new Subscriber();
            $subscriber->setName('New subscriber');
            $subscriber->setChatId($chatId);

            $manager->persist($subscriber);
            $manager->flush();

            return $subscriber;
        } else {
            return $qb;
        }
    }
}
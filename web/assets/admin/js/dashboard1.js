 $(document).ready(function () {
     "use strict";
     // toat popup js
     /*$.toast({
         heading: 'Welcome to Ample admin',
         text: 'Use the predefined ones, or specify a custom position object.',
         position: 'top-right',
         loaderBg: '#fff',
         icon: 'warning',
         hideAfter: 3500,
         stack: 6
     });*/
     //ct-bar-chart
     new Chartist.Bar('#ct-daily-sales', {
         labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
         series: [
    [5, 4, 3, 7, 5, 2, 3]

  ]
     }, {
         axisX: {
             showLabel: false,
             showGrid: false,
             // On the x-axis start means top and end means bottom
             position: 'start'
         },

         chartPadding: {
             top: -20,
             left: 45
         },
         axisY: {
             showLabel: false,
             showGrid: false,
             // On the y-axis start means left and end means right
             position: 'end'
         },
         height: 335,
         plugins: [
    Chartist.plugins.tooltip()
  ]
     });

     // ct-weather
     var chart = new Chartist.Line('#ct-weather', {
         labels: ['1', '2', '3', '4', '5', '6'],
         series: [
    [1, 0, 5, 3, 2, 2.5]

  ]
     }, {
         showArea: true,
         showPoint: false,

         chartPadding: {
             left: -20
         },
         axisX: {
             showLabel: false,
             showGrid: false
         },
         axisY: {
             showLabel: false,
             showGrid: true
         },
         fullWidth: true

     });
     // counter
     $(".counter").counterUp({
         delay: 100,
         time: 1200
     });

     var websocket = WS.connect("wss://notify.ecsv.org.ua:8000");

     websocket.on("socket/connect", function(session){
         //session is an Autobahn JS WAMP session.
         console.log("Successfully Connected!");

         session.subscribe("app/websocket/stats", function(uri, payload){
             var data = JSON.parse(payload.msg);
             var action = data.action;
             console.log(action);
             switch(action) {
                 case 'need_plus_stats':
                     var params = data.params;
                     $('.total-stats').html(params.total);
                     $('.avg-last-hour').html(parseInt(params.avgLastHour));
                     $('.total-last-24-hours').html(params.nowDay);
                     $('.subscribers-total').html(params.subscribersTotal);
                     break;

             }
         });
     });

 });
